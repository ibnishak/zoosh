## Check if tiddlywiki is installed
command -v tiddlywiki >/dev/null 2>&1 || { echo >&2 "Tiddlywiki not found. Aborting"; exit 1; }

## Check if exec directory already exists
if [ -d "./exec" ]; then
		echo "Removing existing exec directory"
		rm -rf ./exec
fi

mkdir exec &&
## pushd is not working in termux
cd ./source &&
echo "Building the executable"		
go build -tags flatfile . &&
mv zoosh ../exec
echo "Building index file"
tiddlywiki index --output ../exec --build index
cd ..
echo "Now you may launch the program as './exec/zoosh -db zsh'"
