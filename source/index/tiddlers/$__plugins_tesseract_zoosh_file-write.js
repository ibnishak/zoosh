/*\
title: $:/plugins/tesseract/zoosh/file-write.js
type: application/javascript
module-type: widget

Action widget to write files

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;
var Apihitter = require("$:/plugins/tesseract/api-hitter/postdata.js")

var FileWriteWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
FileWriteWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
FileWriteWidget.prototype.render = function(parent,nextSibling) {
	this.computeAttributes();
	this.execute();
};

/*
Compute the internal state of the widget
*/
FileWriteWidget.prototype.execute = function() {
	this.actionfname = this.getAttribute("$fname");
	this.actionfdata = this.getAttribute("$fdata");
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
FileWriteWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if($tw.utils.count(changedAttributes) > 0) {
		this.refreshSelf();
		return true;
	}
	return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
FileWriteWidget.prototype.invokeAction = function(triggeringWidget,event) {
    Apihitter.postData(`/zoosh/write`, {fname: this.actionfname, fdata: this.actionfdata})
    .then(data => console.log(JSON.stringify(data))) // JSON-string from `response.json()` call
    .catch(error => console.error(error));
	return true; // Action was invoked
};

exports["action-filewrite"] = FileWriteWidget;

})();