module gitlab.com/ibnishak/zoosh

go 1.12

require (
	gitlab.com/opennota/widdly v0.0.0-20181005151545-1ac16570717a
	golang.org/x/crypto v0.0.0-20190418165655-df01cb2cc480
)
