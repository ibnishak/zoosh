# Zoosh

A GUI to maintain zshrc

## Building from ground up


- Clone the repo and run the build file

```
git clone --depth=1 https://gitlab.com/ibnishak/zoosh.git
cd zoosh
chmod +x ./build.sh
./build.sh
```

